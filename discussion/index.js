// DATA MODELLING

/*
	1. Identify what information we want to gather from the customers in order to determine whether the user's identity is true.

		Why?
		1. To PROPERLY PLAN out what information will be deemed useful.
		2. To lessen the chances or scenarios of having to modify or edit the data stored in the database.
		3. To anticipate how this data/information would relate to each other.
*/

/*
	TASK: Create a Course Booking System for an Institution.

	What are the minimum information would I need to collect from the customers?
	The following information enumerated below would identify the structure of the user in our app.

	User Documents
		1. first name
		2. last name
		3. middle name
		4. username/email address
		5. PIN/password
		6. mobile number
		7. birthdate
		8. gender
		9. isAdmin => role and restriction/limitations that this user would have in our app
		10. dateTimeRegistered => to identify when the student signed up/enrolled in the institution.

	Course/Subjects
		1. name/title
		2. course code
		3. course description
		4. course units
		5. course instructor
		6. isActive => to describe if the course is being offered by the institution
		7. dateTimeCreated => to identify when the course was added to the database.
		8. available slots

	As a full stack web developer (front, backend, database)
	The more information you gather, the more difficult it is to scale and manage the collection. It is more difficult to maintain.
*/

/*
	2. Create an ERD to represent the entities inside the database as well as to describe the relationships amongst them.

		Users can have multiple subjects/courses.
		Subjects can have multiple enrollees.

		A user can have multiple transactions.
		A transaction belong to a single user only.

		A transaction can have multiple courses.
		A course can be part of multiple transactions.
*/

/*
	3. Convert and Translate the ERD into a JSON-like syntax in order to describe the structure of the document inside the collection by creating a MOCK data.

	NOTE: Creating/using a mock data plays an integral role during the testing phase of any app development.

	username: rtgtrhtyhtyj -> this is one of the things you need to avoid.

	You want to be able to utilize the storage in your database.

	Go to this link to generate mock data:
	https://www.mockaroo.com
*/
